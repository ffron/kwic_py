import sys
import time
from functools import reduce
from strsorted import *

def readerInputForTheStdin(input="cmd_line_test/input01.txt"):
	with open(input, "r") as file:
		return[line.strip() for line in file]

def KWIC_generator(docs):
	rotate_generation = lambda words, doc_num:[(doc_num, " ".join(words[i:]), " ".join(words[0:i])) for i in range(0,len(words))]
	splitsTheWords = lambda resultToProcess: reduce(list.__add__, [rotate_generation(item.split(" "), i) for (i, item) in enumerate(resultToProcess)], [])
	apply_sorted_method = lambda tuples, strategy: StrategySorted(tuples).sortedTupleGo()
	print_output = lambda t: print("{0}: {1}   {2}".format(str(t[0]) , t[1], t[2]).rstrip())
	list(map(print_output, apply_sorted_method( splitsTheWords(docs), None ) ))

def main():
	docs = readerInputForTheStdin()
	KWIC_generator( docs )

if __name__ == '__main__':
	start_time = time.time()
	main()
	#print("--- %.4f seconds ---" % (time.time() - start_time))