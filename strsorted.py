import types


class StrategySorted:
	"""docstring for StrategySorted, Strategy to sort"""
	def __init__(self, dictToOrder,func=None):
		self.dictToOrder = dictToOrder
		if func is not None:
			self.sortedTupleGo = types.MethodType(func , self)

	def sortedTupleGo(self):
		return sorted(self.dictToOrder, key=lambda item: item[1])


def sortedTail(self):
	return sorted(self.dictToOrder , key=lambda item: item[2])

				
